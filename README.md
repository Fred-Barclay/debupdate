(Currently experimental) Fork of the Linux Mint Update Manager to Debian.
More details later. Stay tuned!

# Installation:
1) Install prerequisites:
```
sudo apt-get install dconf-gsettings-backend lsb-release inxi python3-apt python3-pycurl synaptic gir1.2-appindicator3-0.1
```

2) Clone and install debUpdate
```
git clone https://gitlab.com/Fred-Barclay/debupdate
cd debupdate
dpkg-buildpackage
cd ..
sudo dpkg -i debupdate*.deb
```

If you run into a broken dependencies message, follow up with
```
sudo apt-get -f install
```
